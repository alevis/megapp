# Mega App Files
 1. :white_check_mark: Hello World
 2. :white_check_mark: Templates
 3. :white_medium_square: Web Forms
 4. :white_medium_square: Database
 5. :white_medium_square: User Logins
 6. :white_medium_square: Profile Page And Avatars
 7. :white_medium_square: Unit Testing
 8. :white_medium_square: Followers, Contacts And Friends
 9. :white_medium_square: Pagination
 10. :white_medium_square: Full Text Search
 11. :white_medium_square: Email Support
 12. :white_medium_square: Facelift
 13. :white_medium_square: Dates and Times
 14. :white_medium_square: L18n and L10n
 15. :white_medium_square: Ajax
 16. :white_medium_square: Debugging, Testing and Profiling
 17. :white_medium_square: Deployment on Linux
 18. :white_medium_square: Deployment oin the Heroku Cloud
